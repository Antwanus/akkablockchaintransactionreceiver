package com.antoonvereecken;

import com.antoonvereecken.web.MyServer;

public class Main {
    public static void main(String[] args) {
        MyServer myServer = new MyServer();
        myServer.run();
    }
}
