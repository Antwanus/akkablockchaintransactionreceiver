package com.antoonvereecken.web;

import akka.NotUsed;
import akka.http.javadsl.model.*;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.stream.typed.javadsl.ActorFlow;
import akka.util.ByteString;
import com.antoonvereecken.actors.TransactionManagerBehavior;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.japi.function.Function;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.antoonvereecken.model.Block;
import com.antoonvereecken.model.Transaction;
import com.antoonvereecken.model.TransactionStatus;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static akka.http.javadsl.server.Directives.*;

public class MyServer {
    ActorSystem<TransactionManagerBehavior.Command> actorSystem;

    private Route createTransactionV2(Transaction transaction) {
        System.out.println("TRANSACTION: " +transaction);
        Source<Transaction, NotUsed> source = Source.single(transaction);

        Flow<Transaction, Integer, NotUsed> newTransactionFlow = ActorFlow.ask(
                actorSystem,
                Duration.ofSeconds(5),
                (transaction1, replyTo) ->  new TransactionManagerBehavior.NewTransactionCommand(transaction, replyTo)
        );
        Flow<Integer, ByteString, NotUsed> convertIntoByteStringFlow = Flow.of(Integer.class)
                .map( id -> {
                    Map<String, Integer> map = Map.of("transactionId", id);
                    return ByteString.fromString(new ObjectMapper().writeValueAsString(map));
            });

        Source<ByteString, NotUsed> graph = source.via(newTransactionFlow).via(convertIntoByteStringFlow);

        return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, graph));
    }
    /* POST transaction: unmarshal json to java & respond with id of the newly created TX */
    Function<HttpRequest, CompletionStage<HttpResponse>> createTransaction = (HttpRequest request) -> {
        System.out.println("ADDING TRANSACTION");
        CompletableFuture<HttpResponse> futureResponse = new CompletableFuture<>();
        Unmarshaller<HttpEntity, Transaction> jsonToTxMapper = Jackson.unmarshaller(Transaction.class);

        CompletionStage<Transaction> txCompletionStage = jsonToTxMapper.unmarshal(request.entity(), actorSystem);

        txCompletionStage.whenComplete( (transaction, throwable1) -> {
            System.out.println("UNMARSHALLING TRANSACTION COMPLETE");
            if (throwable1 != null || transaction == null) {
                System.out.println("THROWABLE != NULL: " +throwable1);
                futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.BAD_REQUEST));
                request.discardEntityBytes(actorSystem);
            }
            else {
                System.out.println("TRANSACTION: " +transaction);
                CompletionStage<Integer> txIdFuture = AskPattern.ask(
                        actorSystem,
                        replyTo -> new TransactionManagerBehavior.NewTransactionCommand(transaction, replyTo),
                        Duration.ofSeconds(5),
                        actorSystem.scheduler()
                );
                txIdFuture.whenComplete((transactionID, throwable2) -> {
                    Map<String, Integer> results = new HashMap<>();
                    results.put("transactionId", transactionID);
                    try {
                        String jsonString = new ObjectMapper().writeValueAsString(results);
                        futureResponse.complete(HttpResponse.create().withStatus(200).withEntity(jsonString));
                        request.discardEntityBytes(actorSystem);
                    } catch (JsonProcessingException e) {
                        System.out.println(e);
                        futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.INTERNAL_SERVER_ERROR));
                        request.discardEntityBytes(actorSystem);
                }});
        }});
        return futureResponse;
    };
    // responds with { "TxStatus":"s", "TxID":"" } for given param("id")
    private Route getTransactionStatusV2(HttpRequest request) {
        CompletableFuture<HttpResponse> futureResponse = new CompletableFuture<>();
        /* mapping param to transactionID */
        int id = 0;
        try {
            id = Integer.parseInt( request.getUri().query().get("id").get());
        } catch (Exception e) {
            futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.BAD_REQUEST)
                    .withEntity("id param is not valid"));
        }
        System.out.println("PARSING ID COMPLETED, RETRIEVING STATUS FOR TRANSACTION_ID: " + id);
        final int finalId = id;

        Source<Integer, NotUsed> source = Source.single(finalId);

        Flow<Integer, TransactionStatus, NotUsed> getTxStatusByIdFlow = ActorFlow.ask(
                actorSystem,
                Duration.ofSeconds(5),
                TransactionManagerBehavior.GetTransactionStatusCommand::new);

        Flow<TransactionStatus, ByteString, NotUsed> convertIntoDTOFlow = Flow.of(TransactionStatus.class)
                .map(status -> {
                    Map<String, Object> result = new HashMap<>();
                    result.put("TransactionId", finalId);
                    result.put("TransactionStatus", status.toString());
                    return ByteString.fromString(new ObjectMapper().writeValueAsString(result));
        });

        Source<ByteString, NotUsed> graph = source.via(getTxStatusByIdFlow).via(convertIntoDTOFlow);

        return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, graph));
    }
    // responds with { "TxStatus":"s", "TxID":"" } for given param("id")
    Function<HttpRequest, CompletionStage<HttpResponse>> getStatusByTxId = request -> {
        CompletableFuture<HttpResponse> futureResponse = new CompletableFuture<>();
        /* mapping param to transactionID */
        int id = 0;
        try {
            id = Integer.parseInt( request.getUri().query().get("id").get());
        } catch (Exception e) {
            futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.BAD_REQUEST)
                    .withEntity("id param is not valid"));
        }
        System.out.println("PARSING ID COMPLETED, RETRIEVING STATUS FOR TRANSACTION_ID: " + id);
        final int finalId = id;
        CompletionStage<TransactionStatus> txStatusFuture = AskPattern.ask(
                actorSystem,
                replyTo -> new TransactionManagerBehavior.GetTransactionStatusCommand(finalId, replyTo),
                Duration.ofSeconds(5),
                actorSystem.scheduler()
        );
        txStatusFuture.whenComplete((transactionStatus, throwable) -> {
            if (throwable != null) {
                System.out.println("THROWABLE != NULL: " +throwable);
                futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.NOT_ACCEPTABLE));
                request.discardEntityBytes(actorSystem);
            } else {
                Map<String, Object> result = new HashMap<>();
                result.put("TransactionId", finalId);
                result.put("TransactionStatus", transactionStatus.toString());
                try {
                    String jsonString = new ObjectMapper().writeValueAsString(result);
                    futureResponse.complete(HttpResponse.create().withStatus(200).withEntity(jsonString));
                    request.discardEntityBytes(actorSystem);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    futureResponse.complete(HttpResponse.create().withStatus(StatusCodes.BAD_REQUEST));
                    request.discardEntityBytes(actorSystem);
                }
            }
        });
        return futureResponse;
    };
    private Route mineBlockV2(HttpRequest request) {
            final String lastHash = request.getUri().query().get("lastHash").get();
            System.out.println("MINING BLOCK WITH LAST_HASH : " + lastHash);

            Flow<String, Block, NotUsed> hashToBlockFlow = ActorFlow.ask(
                    actorSystem,
                    Duration.ofSeconds(5),
                    (hash, blockActorRef) -> new  TransactionManagerBehavior.GenerateBlockCommand(blockActorRef, hash));

            Source<ByteString, NotUsed> source = Source.single(lastHash)
                    .via(   hashToBlockFlow    )
                    .via(   Flow.of(Block.class)
                                .map(block -> {
                                    String result = new ObjectMapper().writeValueAsString(block);
                                    return ByteString.fromString(result);
            }));
            return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, source));
    }
    Function<HttpRequest, CompletionStage<HttpResponse>> mineBlock = request -> {
        CompletableFuture<HttpResponse> response = new CompletableFuture<>();
        try {
            final String lastHash = request.getUri().query().get("lastHash").get();
            System.out.println("MINING BLOCK WITH LAST_HASH : " + lastHash);
            // we need to use AskPattern because this command returns something (a block)
            CompletionStage<Block> blockFuture = AskPattern.ask(
                    actorSystem,
                    replyTo -> new TransactionManagerBehavior.GenerateBlockCommand(replyTo, lastHash),
                    Duration.ofSeconds(5),
                    actorSystem.scheduler()
            );
            response.complete(HttpResponse.create().withStatus(200));
            request.discardEntityBytes(actorSystem);
        } catch (Exception e) {
            response.complete(HttpResponse.create().withStatus(StatusCodes.BAD_REQUEST)
                    .withEntity("lastHash param is not valid"));
            request.discardEntityBytes(actorSystem);
        }
        return response;
    };

    public void run() {
        this.actorSystem = ActorSystem.create(TransactionManagerBehavior.create(), "actorSystem");
        CompletionStage<ServerBinding> server = Http.get(actorSystem)
                .newServerAt("localhost", 8080)
                .bind(createRoute());

        server.whenComplete((serverBinding, throwable) -> {
            if (throwable != null) System.out.println("something went wrong " +throwable);
            else System.out.println("serverBinding.localAddress() ==> " +serverBinding.localAddress());
        });
    }
    private Route createRoute() {
        Unmarshaller<HttpEntity, Transaction> jsonToTxMapper = Jackson.unmarshaller(Transaction.class);
        return pathPrefix("api", ()->
                concat(
                    path("transaction", ()->
                            concat(
//                                        get( ()-> parameter("id", id -> handle(getStatusByTxId))),
                                    get( ()-> extractRequest(this::getTransactionStatusV2)),
//                                        post( () -> handle(createTransaction))
                                    post( ()-> entity(jsonToTxMapper, this::createTransactionV2))
                            )),
                    path("mining", () ->
//                            put( () -> handle(mineBlock))
                        put( () -> extractRequest(this::mineBlockV2))
                    )));
    }
}
